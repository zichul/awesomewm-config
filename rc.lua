-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local vicious = require("vicious")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                    title = "Oops, there were errors during startup!",
                    text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                        title = "Oops, an error happened!",
                        text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init( awful.util.getdir("config") .. "/themes/dremora/theme.lua" )

-- This is used later as the default terminal and editor to run.
terminal = "termite"
filemanager = "pcmanfm"
browser = "firefox"
editor = os.getenv("EDITOR") or "gvim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.floating,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}


-- {{{ Tags
layouts = awful.layout.layouts
 tags = {
    names  = { "Home", "Media", "Edit", "VM", "Mail", "Chat"},
    layout = { layouts[2], layouts[3], layouts[2], layouts[2], layouts[2], layouts[2] }
 }

 for s = 1, screen.count() do
    tags[s] = awful.tag(tags.names, s, tags.layout)
 end
 -- }}}


-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
  { "manual", terminal .. " -e man awesome" },
  { "edit config", editor_cmd .. " " .. awesome.conffile },
  { "restart", awesome.restart },
  { "quit", awesome.quit }
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
  awful.button({ }, 1, awful.tag.viewonly),
  awful.button({ modkey }, 1, awful.client.movetotag),
  awful.button({ }, 3, awful.tag.viewtoggle),
  awful.button({ modkey }, 3, awful.client.toggletag),
  awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
  awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
)
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
  awful.button({ }, 1, function (c)
    if c == client.focus then
      c.minimized = true
    else
      -- Without this, the following
      -- :isvisible() makes no sense
      c.minimized = false
      if not c:isvisible() then
        awful.tag.viewonly(c:tags()[1])
      end
      -- This will also un-minimize
      -- the client, if needed
      client.focus = c
      c:raise()
    end
  end),
  awful.button({ }, 3, function ()
    if instance then
      instance:hide()
      instance = nil
    else
      instance = awful.menu.clients({
        theme = { width = 250 }
      })
    end
  end),
  awful.button({ }, 4, function ()
    awful.client.focus.byidx(1)
  end),
  awful.button({ }, 5, function ()
    awful.client.focus.byidx(-1)
  end))

-- Create Vicious widgets

-- Date
datewidget = wibox.widget.textbox()
vicious.register(datewidget, vicious.widgets.date, "%b %d %a, %R", 60)

-- Memory usage
memwidget = wibox.widget.textbox()
vicious.register(memwidget, vicious.widgets.mem, "$1% ", 13)

-- create gradient color for test
function gradient(color, to_color, min, max, value)
    local function color2dec(c)
        return tonumber(c:sub(2,3),16), tonumber(c:sub(4,5),16), tonumber(c:sub(6,7),16)
    end

    local factor = 0
    if (value >= max) then
        factor = 1
    elseif (value > min) then
        factor = (value - min) / (max - min)
    end

    local red, green, blue = color2dec(color)
    local to_red, to_green, to_blue = color2dec(to_color)

    red   = math.floor( red   + (factor * (to_red   - red)))
    green = math.floor( green + (factor * (to_green - green)))
    blue  = math.floor( blue  + (factor * (to_blue  - blue)))

    -- dec2color
    return string.format("#%02x%02x%02x", red, green, blue)
end

-- cpuwidget = widget({ type = "textbox" })
cpuwidget = wibox.widget.textbox()

vicious.register(cpuwidget, vicious.widgets.cpu,
function (widget, args)
  local text = ""
  -- list only real cpu cores
  -- for i=1,#args do
  for i=1,1 do
    -- alerts, if system is stressed
    if args[i] > 50 then
      -- from light green to light red
      local color = gradient("#AECF96","#FF5656",50,100,args[i])
      args[i] = string.format("<span color='%s'>%s</span>", color, args[i])
    end

    -- append to list
    if i > 2 then text = text .. args[i].."% "
    else text = text .. args[i].."% " end
  end

  return text
end )

--Caching for CPU widget
-- vicious.cache(vicious.widgets.cpu)

-- CPU temperature
thermalwidget  = wibox.widget.textbox()
vicious.register(thermalwidget, vicious.widgets.thermal,
-- function (widget, args)
  -- local color = gradient("#AECF96","#FF5656",30,80,args[1])
  -- local text = string.format("<span color='%s'>%s°C</span> ", color, args[1])
  -- return text
-- end ,
 "$1",
5, { "thermal_zone0", "sys", "/sys/class/thermal/thermal_zone0/temp"} )

vicious.cache(vicious.widgets.thermal)

-- Round
function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

-- Function to transform kb value into less than 1000 number with unit
function getByteString(number)
  number = tonumber(number)
  local acronims = { "kB", "MB", "GB"}
  for k,v in pairs(acronims) do
    if number < 1024 then
      return (round(number,2) .. v)
    else
      number = number / 1024
    end
  end
end

-- Format net widgets
function formatNetInterface(up, down, icon_internal, icon)
  local text = ""
  local colors = {"#CC9393", "#7F9F7F"}
  up = tonumber(up)
  down = tonumber(down)
  if up > 0 or down > 0 then
  -- if true then
    local down_bytes = getByteString(down)
    local up_bytes = getByteString(up)
    -- text = string.format( '<span color="%s">%s➘</span> <span color="%s">%s➚</span> ',
    -- colors[1], down_bytes,
    -- colors[2], up_bytes)
    text = string.format( '%s➘ %s➚ ', down_bytes, up_bytes)
    icon:set_widget(icon_internal)
  else
    icon:set_widget(nil)
  end
  return text
end

--Caching for net widget
vicious.cache(vicious.widgets.net)

--Ethernet icon
etherneticon_internal = wibox.widget.imagebox()
etherneticon_internal:set_image(awful.util.getdir("config") .. "/icons/ethernet.png")
etherneticon = wibox.layout.margin()

--Ethernet widget
ethernetwidget = wibox.widget.textbox()
vicious.register(ethernetwidget , vicious.widgets.net, function(widget, args)
  return formatNetInterface(args["{enp4s0 up_kb}"], args["{enp4s0 down_kb}"], etherneticon_internal, etherneticon)
end, 3)

-- Wifi icon
wifiicon_internal = wibox.widget.imagebox()
wifiicon_internal:set_image(awful.util.getdir("config") .. "/icons/wifi.png")
wifiicon = wibox.layout.margin()


--Wifi widget
wifiwidget = wibox.widget.textbox()
vicious.register(wifiwidget , vicious.widgets.net, function(widget, args)
  return formatNetInterface(args["{wlp5s0 up_kb}"], args["{wlp5s0 down_kb}"], wifiicon_internal, wifiicon)
end, 3)

-- AC icon
acicon_internal = wibox.widget.imagebox()
acicon_internal:set_image(awful.util.getdir("config") .. "/icons/ac.png")

-- Bat icon
baticon_internal = wibox.widget.imagebox()
baticon_internal:set_image(awful.util.getdir("config") .. "/icons/bat.png")

-- ACBat icon
batacicon_internal = wibox.widget.imagebox()
batacicon_internal:set_image(awful.util.getdir("config") .. "/icons/batac.png")

baticon = wibox.layout.margin()

--Battery widget
batterywidget = wibox.widget.textbox()
vicious.register(batterywidget , vicious.widgets.bat,
function(widget, args)
  -- return args[3]
  h,m = string.match(args[3],'(%d%d):(%d%d)')
  if args[1] == "+" then
    baticon:set_widget(batacicon_internal)
  elseif args[1] == "−" and h ~= nil and m ~= nil then
    baticon:set_widget(baticon_internal)
  else
    baticon:set_widget(acicon_internal)
    return ""
  end
  local text = string.format("%s%% %sh%sm ",args[2],h,m)
  return text
end, 30, "BAT0")

-- Create widget icons
cpuicon = wibox.widget.imagebox()
cpuicon:set_image(awful.util.getdir("config") .. "/icons/processor.png")

memoryicon = wibox.widget.imagebox()
memoryicon:set_image(awful.util.getdir("config") .. "/icons/memory.png")

thermalicon = wibox.widget.imagebox()
thermalicon:set_image(awful.util.getdir("config") .. "/icons/thermometer.png")


for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])
    left_layout:add(mylayoutbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then
      right_layout:add(cpuicon)
      right_layout:add(cpuwidget)
      right_layout:add(thermalicon)
      right_layout:add(thermalwidget)
      right_layout:add(memoryicon)
      right_layout:add(memwidget)
      right_layout:add(etherneticon)
      right_layout:add(ethernetwidget)
      right_layout:add(wifiicon)
      right_layout:add(wifiwidget)
      right_layout:add(baticon)
      right_layout:add(batterywidget)
      right_layout:add(wibox.widget.systray())
    end
    right_layout:add(datewidget)


    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey,           }, "x", function () awful.util.spawn(filemanager) end),
    awful.key({ modkey,           }, "c", function () awful.util.spawn(terminal .. " -e ranger") end),
    awful.key({ modkey,           }, "b", function () awful.util.spawn(browser) end),
    awful.key({ modkey,           }, "v", function () awful.util.spawn_with_shell("virtualbox --startvm Windows10") end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    -- awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    -- awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Shift" }, "l", function () awful.client.incwfact(-0.05) end),
    awful.key({ modkey, "Shift" }, "h", function () awful.client.incwfact( 0.05) end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1) end),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "a",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end),
    awful.key({ }, "Print", function () awful.util.spawn("shutter -e -s") end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end),
    awful.key({ modkey,           }, "i",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            -- awful.client.focus.byidx( 1)
            -- awful.client.focus.history.previous()
            -- if client.focus then client.focus:raise() end
            c.minimized = true
        end),
    -- all minimized clients are restored
    awful.key({ modkey, "Shift"   }, "n",
        function()
            local tag = awful.tag.selected()
                for i=1, #tag:clients() do
                    tag:clients()[i].minimized=false
            end
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.toggletag(tag)
                          end
                      end
                  end))
end

-- Get active outputs
local function outputs()
   local outputs = {}
   local xrandr = io.popen("xrandr -q")
   if xrandr then
      for line in xrandr:lines() do
        output = line:match("^([%w-]+) connected ")
        if output then
          outputs[#outputs + 1] = output
        end
      end
      xrandr:close()
   end

   return outputs
end

local function arrange(out)
   -- We need to enumerate all the way to combinate output. We assume
   -- we want only an horizontal layout.
   local choices  = {}
   local previous = { {} }
   for i = 1, #out do
      -- Find all permutation of length `i`: we take the permutation
      -- of length `i-1` and for each of them, we create new
      -- permutations by adding each output at the end of it if it is
      -- not already present.
      local new = {}
      for _, p in pairs(previous) do
        for _, o in pairs(out) do
          if not awful.util.table.hasitem(p, o) then
              new[#new + 1] = awful.util.table.join(p, {o})
          end
        end
      end
      choices = awful.util.table.join(choices, new)
      previous = new
   end

   return choices
end

-- Build available choices
local function menu()
   local menu = {}
   local out = outputs()
   local choices = arrange(out)

   for _, choice in pairs(choices) do
      local cmd = "xrandr"
      -- Enabled outputs
      for i, o in pairs(choice) do
        cmd = cmd .. " --output " .. o .. " --auto"
        if i > 1 then
          cmd = cmd .. " --right-of " .. choice[i-1]
        end
      end
      -- Disabled outputs
      for _, o in pairs(out) do
        if not awful.util.table.hasitem(choice, o) then
          cmd = cmd .. " --output " .. o .. " --off"
        end
      end

      local label = ""
      if #choice == 1 then
        label = 'Only <span weight="bold">' .. choice[1] .. '</span>'
      else
        for i, o in pairs(choice) do
          if i > 1 then label = label .. " + " end
          label = label .. '<span weight="bold">' .. o .. '</span>'
        end
      end

      menu[#menu + 1] = { label, cmd, "/usr/share/icons/Tango/32x32/devices/display.png"}
   end

   return menu
end

-- Display xrandr notifications from choices
local state = { iterator = nil,
		timer = nil,
		cid = nil }
local function xrandr()
   -- Stop any previous timer
   if state.timer then
      state.timer:stop()
      state.timer = nil
   end

   -- Build the list of choices
   if not state.iterator then
      state.iterator = awful.util.table.iterate(menu(),
					function() return true end)
   end

   -- Select one and display the appropriate notification
   local next  = state.iterator()
   local label, action, icon
   if not next then
      label, icon = "Keep the current configuration", "/usr/share/icons/Tango/32x32/devices/display.png"
      state.iterator = nil
   else
      label, action, icon = unpack(next)
   end
   state.cid = naughty.notify({ text = label,
				icon = icon,
				timeout = 4,
				screen = mouse.screen, -- Important, not all screens may be visible
				font = "Free Sans 18",
				replaces_id = state.cid }).id

   -- Setup the timer
   state.timer = timer { timeout = 4 }
   state.timer:connect_signal("timeout", function()
                              state.timer:stop()
                              state.timer = nil
                              state.iterator = nil
                              if action then
                                awful.util.spawn(action, false)
                              end
                            end)
   state.timer:start()
end

globalkeys = awful.util.table.join(
   globalkeys,
   awful.key({ modkey }, "e", xrandr))
clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    -- Start windows as slave
    { rule = { }, properties = { },
      callback = awful.client.setslave },
    -- { rule = { class = "MPlayer" },
      -- properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "xmessage" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    { rule = { class = "Thunderbird" },
      properties = { tag = tags[1][5] } },
    { rule = { class = "transmission" },
      properties = { tag = tags[1][2] } },
    { rule = { class = "Vlc" },
      properties = { tag = tags[1][2] } },
    { rule = { class = "Xchat" },
      properties = { tag = tags[1][5] } },
    { rule = { class = "Clementine" },
      properties = { tag = tags[1][2] } },
    { rule = { class = "Pidgin" },
      properties = { tag = tags[1][6] } },
    { rule = { class = "VirtualBox" },
      properties = { tag = tags[1][4] } },
    { rule = { class = "qTox" },
      properties = { tag = tags[1][6] } },
    { rule = { class = "qemu-system-x86_64" },
      properties = { tag = tags[1][4] } },


}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    if not awesome.startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    elseif not c.size_hints.user_position and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count change
        awful.placement.no_offscreen(c)
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

-- Enable sloppy focus
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

-- blink window border
client.connect_signal("focus", function(c)
  c.border_color = beautiful.border_focus
  local blink_timer = timer { timeout = 0.2 }
  blink_timer:connect_signal("timeout", function()
    blink_timer:stop()
    blink_timer = nil
    if c.valid then 
      c.border_color = beautiful.border_normal
    end
  end)
  blink_timer:start()
end)

client.connect_signal("unfocus", function(c)
  c.border_color = beautiful.border_normal
end)

-- }}}
--
-- awful.util.spawn_with_shell("run_once volumeicon &")
awful.util.spawn_with_shell("run_once nm-applet &")
-- awful.util.spawn_with_shell("run_once firefox &")
-- awful.util.spawn(browser)
awful.util.spawn_with_shell("run_once thunderbird &")
awful.util.spawn_with_shell("run_once mpd &")
awful.util.spawn_with_shell("run_once pasystray &")
awful.util.spawn_with_shell("run_once compton &")
awful.util.spawn_with_shell("run_once xbindkeys &")
-- awful.util.spawn_with_shell("run_once syndaemon -t -k -i 2 -d") -- not working?
