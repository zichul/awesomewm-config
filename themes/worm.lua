--------------------------------
--    "Worm" awesome theme    --
--------------------------------
--   Inspired by Adobe Kuler  --
--------------------------------

-- {{{ Main
theme = {}
theme.default_themes_path = "/usr/share/awesome/themes"
theme.wallpaper = "~/.config/awesome/themes/landscape.png"
-- }}}

-- {{{ Styles
theme.font          = "Profont 8"

-- {{{ Colors
theme.fg_normal     = "#54534B"
theme.fg_focus      = "#54534B"
theme.fg_urgent     = "#562630"
--theme.fg_minimize = "#000000"
theme.bg_normal     = "#7D8C7C"
theme.bg_focus      = "#BCBDA5"
theme.bg_urgent     = "#DACEB1"
--theme.bg_minimize = "#81654F"
-- }}}

-- {{{ Borders
theme.border_width  = "1"
theme.border_normal = "#7D8C7C"
theme.border_focus  = "#BCBDA5"
theme.border_marked = "#562630"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#BCBDA5"
theme.titlebar_bg_normal = "#BCBDA5" --"#7D8C7C"
-- }}}
-- }}}

-- {{{ Icons / Misc.
-- {{{ Taglist
theme.taglist_squares_sel   = theme.default_themes_path.."/zenburn/taglist/squarefz.png"
theme.taglist_squares_unsel = theme.default_themes_path.."/zenburn/taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = theme.default_themes_path.."/zenburn/awesome-icon.png"
theme.menu_submenu_icon      = theme.default_themes_path.."/default/submenu.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = theme.default_themes_path.."/zenburn/layouts/tile.png"
theme.layout_tileleft   = theme.default_themes_path.."/zenburn/layouts/tileleft.png"
theme.layout_tilebottom = theme.default_themes_path.."/zenburn/layouts/tilebottom.png"
theme.layout_tiletop    = theme.default_themes_path.."/zenburn/layouts/tiletop.png"
theme.layout_fairv      = theme.default_themes_path.."/zenburn/layouts/fairv.png"
theme.layout_fairh      = theme.default_themes_path.."/zenburn/layouts/fairh.png"
theme.layout_spiral     = theme.default_themes_path.."/zenburn/layouts/spiral.png"
theme.layout_dwindle    = theme.default_themes_path.."/zenburn/layouts/dwindle.png"
theme.layout_max        = theme.default_themes_path.."/zenburn/layouts/max.png"
theme.layout_fullscreen = theme.default_themes_path.."/zenburn/layouts/fullscreen.png"
theme.layout_magnifier  = theme.default_themes_path.."/zenburn/layouts/magnifier.png"
theme.layout_floating   = theme.default_themes_path.."/zenburn/layouts/floating.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = theme.default_themes_path.."/zenburn/titlebar/close_focus.png"
theme.titlebar_close_button_normal = theme.default_themes_path.."/zenburn/titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active  = theme.default_themes_path.."/zenburn/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = theme.default_themes_path.."/zenburn/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = theme.default_themes_path.."/zenburn/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = theme.default_themes_path.."/zenburn/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = theme.default_themes_path.."/zenburn/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = theme.default_themes_path.."/zenburn/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = theme.default_themes_path.."/zenburn/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = theme.default_themes_path.."/zenburn/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = theme.default_themes_path.."/zenburn/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = theme.default_themes_path.."/zenburn/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = theme.default_themes_path.."/zenburn/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = theme.default_themes_path.."/zenburn/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = theme.default_themes_path.."/zenburn/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = theme.default_themes_path.."/zenburn/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.default_themes_path.."/zenburn/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.default_themes_path.."/zenburn/titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme
